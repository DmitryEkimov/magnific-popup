	Config = {
		"item": {
			"allow360X":true,
			"interaction": "mousemove", // "drag"
			"impetus": "true", //"false"
			"containerSelector": ".pro360",
			"autoplay": {"interval": 100, "bounce": true},//frame timeout
			"folder":"500/",
			"zoomfolder":"2000/",
			"file": "{XXX}.jpg", // {-XXX}.jpg for order inversion
			//"invertScroll": true, // uncomment for inverting scroll direction
			"type": "row",
			"startX":2,
			"minX": 1,
			"maxX": 56,
			"width": 320,
			"height": 320,
			"zoomfactor": 4
		}
	}
