$(document).ready(function(){
    
    $(document).on('click', '.material-item-slide', function(e){
        e.preventDefault();
        var position = $(this).offset();
        var height = $(this).closest('.swiper-container').outerHeight();
        var left = position.left;
        
        if((position.left + $('.material-modal').outerWidth()) > $(window).outerWidth()){
            left = (position.left - $('.material-modal').outerWidth() + $(this).outerWidth())
            $('.material-modal').addClass('material-modal-left');
        }else{
            $('.material-modal').removeClass('material-modal-left');
        }
        
        $('.material-modal-inner').html($(this).find('.material-item-slide-inner').html());
        $('.material-modal').css({left:left, top:(position.top+height)}).fadeIn();
    });
    $(document).on('click', '.material-modal-close', function(e){
        e.preventDefault();
        $(this).closest('.material-modal').fadeOut();
    });
    $(document).on('click', '.gallery-point-modal-close', function(e){
        e.preventDefault();
        $(this).closest('.gallery-point-modal').fadeOut();
        $(this).closest('.gallery-point').find('.gallery-point-inner').removeClass('active');
    });
    
    $(document).on('click', '.gallery-point-inner', function(e){
        e.preventDefault();
        
        $('.gallery-point-inner').not(this).removeClass('active');
        $('.gallery-point-inner').not(this).siblings('.gallery-point-modal').fadeOut();
        
        var position = $(this).closest('.gallery-point').position();
        $(this).toggleClass('active');
        
        if((position.left + $(this).siblings('.gallery-point-modal').outerWidth()) > $(this).closest('.gallery-top-img').outerWidth()){
            $(this).siblings('.gallery-point-modal').addClass('gallery-point-modal-left');
        }else{
            $(this).siblings('.gallery-point-modal').removeClass('gallery-point-modal-left');
        }
        
        if((position.top + $(this).siblings('.gallery-point-modal').outerHeight()) > $(this).closest('.gallery-top-img').outerHeight()){
            $(this).siblings('.gallery-point-modal').addClass('gallery-point-modal-top');
        }else{
            $(this).siblings('.gallery-point-modal').removeClass('gallery-point-modal-top');
        }
        
        $(this).siblings('.gallery-point-modal').fadeToggle();
    });

if (typeof Swiper !== 'undefined')  {    

    var galleryThumbs = new Swiper('.gallery-thumbs', {
      direction: 'vertical',
      spaceBetween: 10,
      slidesPerView: 3,
      freeMode: false,
      watchSlidesVisibility: true,
      watchSlidesProgress: true,
    });
    var galleryTop = new Swiper('.gallery-top', {
      spaceBetween: 10,
    //   autoHeight: true,
      navigation: {
        nextEl: '.swiper-button-next-g',
        prevEl: '.swiper-button-prev-g',
      },
      pagination: {
        el: '.swiper-pagination-g',
        clickable: true,
      },
      thumbs: {
        swiper: galleryThumbs
      },
      breakpoints: {
        991: {
            autoHeight: true,
        }
      }
    });
    
    var swiper = new Swiper('.materials', {
        slidesPerView: 9,
        spaceBetween: 30,
        breakpoints: {
            350: {
                slidesPerView: 4,
                spaceBetween: 10
            },
            450: {
                slidesPerView: 5,
                spaceBetween: 10
            },
            600: {
                slidesPerView: 7,
                spaceBetween: 10
            },
            767: {
                slidesPerView: 9,
                spaceBetween: 10
            },
            991: {
                slidesPerView: 6,
                spaceBetween: 20,
            },
            1199: {
                slidesPerView: 7,
                spaceBetween: 30
            }
        },
    //   centeredSlides: true,
      navigation: {
        nextEl: '.swiper-button-next-material',
        prevEl: '.swiper-button-prev-material',
      },
    });
}

init_mask_phone();

$(document).on('click', 'a[data-design]', function(e){
    e.preventDefault();
    $($(this).attr('href')).find('input[name="cnt2cnt[31]"]').val($(this).attr('data-design'));
    $($(this).attr('href')).modal();
});

$('#add').on('click', function() {
	var num = $(".tables").map(function() {
	   	return $(this).data('table-id');
	}).get();
	var highest = Math.max.apply(Math, num);
	if(highest == '-Infinity') var highest = 0;
	var next_table = highest + 1;
	
	$('.sklad').prepend('<div class="tables" data-table-id="'+ next_table + '"><div class="col-md-4"><div class="form-group"><input name="q['+next_table+']" type="text" class="form-control added" placeholder="Артикул или наименование" /><a class="remove"><i class="glyphicon glyphicon-remove"></i></a></div></div><div class="col-md-8" id="result'+next_table+'"></div></div>');
	$('.last').first().removeClass('last');
	return false;
});
		
$('body').on('click', '.remove', function() {
	$(this).parents('.tables').remove();
	return false;
});

$('body').keypress(function(e){
        if(e.which == 13){
            $('.search').click();
        }
    });
		
$('.search').on('click', function() {
   if(trim($('input[name^="q[1]"]').val()) == '') {
	$('input[name^="q[1]"]').val('');
   }  
    var data = $('input[name^="q"]').serialize();
	$('input[name^="q"]').each(function(key, value){
		if(trim($(this).val()) == '' && $(this).attr('name') != 'q[1]') {
			$(this).parents('.tables').remove();
		}
	});
    $.ajax({
        type: 'POST',
        url: '/sklad/search.php',
        data: data,
	dataType: "json",
        success: function(data) {
		$('.result-count').remove();
		$('.result-table').remove();
		if(data != null) {
		$.each(data, function(key, value) {
			if(value == '') {
				$('<div class="col-md-8 result-count" style="color: red">Совпадений не найдено</div>').insertAfter("#result" + key);
			} else {
				$(value).insertAfter("#result" + key);
			}
		});
		$(".tsearch").tablesorter();
	}
        }
    });
});

function trim(str, chars) { 
	return ltrim(rtrim(str, chars), chars); 
} 
 
function ltrim(str, chars) { 
	chars = chars || "\\s"; 
	return str.replace(new RegExp("^[" + chars + "]+", "g"), ""); 
} 
 
function rtrim(str, chars) { 
	chars = chars || "\\s"; 
	return str.replace(new RegExp("[" + chars + "]+$", "g"), ""); 
}

    //top btn
	if($(window).width() > 768){
        $(window).scroll(function(){
            if($(window).scrollTop() > 100){
                $('#top').fadeIn();
            }
    		if ($(this).scrollTop() > 100) {
    			$('#top').fadeIn();
    		} else {
    			$('#top').fadeOut();
    		}
    	});
    }
	$('#top').click(function(){
		$('html, body').animate({scrollTop : 0},800);
		return false;
	});
    
    $( ".radio-styled.star" ).on('click', function(){
        $(this).closest('.form-group').find(".radio-styled.star").removeClass('hover');
        $(this).addClass('hover').prevAll().addClass('hover');    
    });
    
/*$( ".radio-styled.star" )
  .mouseenter(function() {
    $(this).addClass('hover').prevAll().addClass('hover');
  })
  .mouseleave(function() {
    $(this).removeClass('hover').prevAll().removeClass('hover');
  });*/    
    function stickyFooter(){
        var footerHeight = $('.footer-wrapper').height();
        var footerIndent = 20;
        var bodyMargin = footerHeight + footerIndent;
        $('body').css('margin-bottom' , bodyMargin);
    }

    function headerCatalogArrow (){

        $('.header-catalog>li.active').stop();
        $('.header-catalog-arrow').stop();
        $('.header-catalog>li').stop();
        $('.header-catalog>li.hovered').stop();

	if($('.header-catalog>li.hovered').length) {
        var e = $('.header-catalog>li.hovered'),
            eCoordX = e.offset().left,
            eWidth = e.outerWidth(),
            eWidthCenter = eWidth/ 2,
            eArrowHalfWidth = 18.5,
            arrowCoordX = eCoordX + eWidthCenter - eArrowHalfWidth,
            arrowLeft = arrowCoordX + 'px';
        $('.header-catalog-arrow').css('left', arrowLeft);
	}
    }

    function headerCatalogSubMenuWidth (){
        var wrapWidth = $('.header-catalog-wrap').outerWidth(),
            menuIndent = $('.header-catalog').offset().left,
            subMenuCoordX = -menuIndent,
            subMenuWidth = wrapWidth;
        $('.header-catalog>li>ul').css({
            "left":subMenuCoordX,
            "width":subMenuWidth,
            "padding-left": menuIndent,
            "padding-right": menuIndent
        })
    }


    var timer = 0;

//    $('.header-catalog>li>a').hover(
    $('.header-catalog>li').hover(
        function() {
            clearTimeout(timer);
//            var currentLi = $(this).closest('li');
            var currentLi = $(this);
            if ((currentLi.hasClass('hovered'))!=true){
                $('.header-catalog li').removeClass('hovered').clearQueue();
                currentLi.addClass('hovered');
                headerCatalogArrow();
                headerCatalogSubMenuWidth();
                headerCatalogSubMenuItemDirection();
            }
        },
        function(){
            clearTimeout(timer);

            timer = setTimeout(function(){
                var e = $('.header-catalog>li.active'),
                    eCoordX = e.offset().left,
                    eWidth = e.outerWidth(),
                    eWidthCenter = eWidth/ 2,
                    eArrowHalfWidth = 18.5,
                    arrowCoordX = eCoordX + eWidthCenter - eArrowHalfWidth,
                    arrowLeft = arrowCoordX + 'px';
                $('.header-catalog-arrow').css('left', arrowLeft);
                $('.header-catalog>li').removeClass('hovered');
                e.addClass('hovered');
            }, 300);
            

        }
    );

    $('.header-catalog>li>a').click(
        function() {
            var currentLi = $(this).closest('li'),
                headerWidth = $('.header').outerWidth();
            if (headerWidth < 992){
                if (currentLi.hasClass('active-sm')){
                    currentLi.removeClass('active-sm');
                    currentLi.find('ul').slideUp();
                    $(this).find('.glyphicon').removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
                    headerCatalogSubMenuItemDirection();
                } else {
                    currentLi.addClass('active-sm');
                    currentLi.find('ul').slideDown();
                    $(this).find('.glyphicon').removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
                    headerCatalogSubMenuItemDirection();
                }
            }
        }
    );

    $('.main-slider-item').each(function(){
        var imageUrl = $(this).find('.main-slider-item-bg').attr('src');
        var imageUrlCss = 'url("'+imageUrl+'")';
        $(this).css('background-image', imageUrlCss);
    });
    $('.header-catalog-img-wrap').each(function(){
        var imageUrl = $(this).find('img').attr('src');
        var imageUrlCss = 'url("'+imageUrl+'")';
        $(this).css('background-image', imageUrlCss);
    });
    $('.product-item-lg-img').each(function(){
        var imageUrl = $(this).find('img').attr('src');
        var imageUrlCss = 'url("'+imageUrl+'")';
        $(this).css('background-image', imageUrlCss);
    });
    $('.product-section-item-img').each(function(){
        var imageUrl = $(this).find('img').attr('src');
        var imageUrlCss = 'url("'+imageUrl+'")';
        $(this).css('background-image', imageUrlCss);
    });
    $('.product-item-sm-img-wrap').each(function(){
        var imageUrl = $(this).find('img').attr('src');
        var imageUrlCss = 'url("'+imageUrl+'")';
        $(this).css('background-image', imageUrlCss);
    });
    $('.article-img').each(function(){
        var imageUrl = $(this).find('img').attr('src');
        var imageUrlCss = 'url("'+imageUrl+'")';
        $(this).css('background-image', imageUrlCss);
    });
    $('.shop-sm-item-img').each(function(){
        var imageUrl = $(this).find('img').attr('src');
        var imageUrlCss = 'url("'+imageUrl+'")';
        $(this).css('background-image', imageUrlCss);
    });
    $('.shop-item-img').each(function(){
        var imageUrl = $(this).find('img').attr('src');
        var imageUrlCss = 'url("'+imageUrl+'")';
        $(this).css('background-image', imageUrlCss);
    });
    $('.news-item-img').each(function(){
        var imageUrl = $(this).find('img').attr('src');
        var imageUrlCss = 'url("'+imageUrl+'")';
        $(this).css('background-image', imageUrlCss);
    });
    $('.detail-gallery-item').each(function(){
        var imageUrl = $(this).find('img').attr('src');
        var imageUrlCss = 'url("'+imageUrl+'")';
        $(this).css('background-image', imageUrlCss);
    });
    /*
    $('.select-color-item-img').each(function(){
        var imageUrl = $(this).find('img').attr('src');
        var imageUrlCss = 'url("'+imageUrl+'")';
        $(this).css('background-image', imageUrlCss);
    });
    */
    $('.detail-img').each(function(){
        var imageUrl = $(this).find('img').attr('src');
        var imageUrlCss = 'url("'+imageUrl+'")';
        $(this).css('background-image', imageUrlCss);
    });
    $('.tile-item-img').each(function(){
        var imageUrl = $(this).find('img').attr('src');
        var imageUrlCss = 'url("'+imageUrl+'")';
        $(this).css('background-image', imageUrlCss);
    });
    $('.interior-item-img').each(function(){
        var imageUrl = $(this).find('img').attr('src');
        var imageUrlCss = 'url("'+imageUrl+'")';
        $(this).css('background-image', imageUrlCss);
    });

    //close-open filter-item
    $(".filter-top-select-caption").click(function(){;
        $(this).closest(".filter-top-select").toggleClass("active");
    });
    $(".filter-top-select-content-btn .btn-default").click(function(){;
        $(this).closest(".filter-top-select").toggleClass("active");
    });

    function headerCatalogSubMenuItemDirection (){
        $('.header-catalog>li>ul').each(function(){
            Number.prototype.Crop = function (x){
                var s = this+'', a = s.split('.');
                a[1]=a[1]||'';
                return parseFloat(a[0]+'.'+a[1].substring(0,x));
            };
            var rowWidth = $(this).width();
            var itemWidth = $(this).find('li a').outerWidth();
            var itemInTheRow = (rowWidth/itemWidth).Crop(0);
            $(this).find('li').each(function(){
                var itemIndex = (($(this).index()));
                var rowNum = ((itemIndex/itemInTheRow).Crop(0)+1);
                $(this).removeClass('item-rtl');
                if (rowNum%2){
                    $(this).addClass('item-rtl');
                } else {
                    $(this).removeClass('item-rtl');
                }
            });
        });
    };

    function filterSly (){
        $('#filter-1').find('.filter-top-select-content-list').css('height','auto');
        var innerWrapHeight = $('.filter-top-select-content-list').height();
        var options = {
            itemNav: 'basic',
            speed: 300,
            scrollBar: $('#filter-scrollbar-1'),
            dragHandle: true,
            dynamicHandle: true,
            minHandleSize: 50,
            syncSpeed: 0.5,
            mouseDragging: true,
            touchDragging: true,
            swingSpeed: 0.4,
            scrollSource: null,
            scrollBy:     1,
            scrollHijack: 100,
            scrollTrap:   false
        };
        $('#filter-1').find('.filter-top-select-content-list').css('height',innerWrapHeight);
        $('#filter-1').sly(options);

        $('#filter-2').find('.filter-top-select-content-list').css('height','auto');
        var innerWrapHeight = $('.filter-top-select-content-list').height();
        var options = {
            itemNav: 'basic',
            speed: 300,
            scrollBar: $('#filter-scrollbar-2'),
            dragHandle: true,
            dynamicHandle: true,
            minHandleSize: 50,
            syncSpeed: 0.5,
            mouseDragging: true,
            touchDragging: true,
            swingSpeed: 0.4,
            scrollSource: null,
            scrollBy:     1,
            scrollHijack: 100,
            scrollTrap:   false
        };
        $('#filter-2').find('.filter-top-select-content-list').css('height',innerWrapHeight);
        $('#filter-2').sly(options);

        $('#filter-3').find('.filter-top-select-content-list').css('height','auto');
        var innerWrapHeight = $('.filter-top-select-content-list').height();
        var options = {
            itemNav: 'basic',
            speed: 300,
            scrollBar: $('#filter-scrollbar-3'),
            dragHandle: true,
            dynamicHandle: true,
            minHandleSize: 50,
            syncSpeed: 0.5,
            mouseDragging: true,
            touchDragging: true,
            swingSpeed: 0.4,
            scrollSource: null,
            scrollBy:     1,
            scrollHijack: 100,
            scrollTrap:   false
        };
        $('#filter-3').find('.filter-top-select-content-list').css('height',innerWrapHeight);
        $('#filter-3').sly(options);

        $('#filter-4').find('.filter-top-select-content-list').css('height','auto');
        var innerWrapHeight = $('.filter-top-select-content-list').height();
        var options = {
            itemNav: 'basic',
            speed: 300,
            scrollBar: $('#filter-scrollbar-4'),
            dragHandle: true,
            dynamicHandle: true,
            minHandleSize: 50,
            syncSpeed: 0.5,
            mouseDragging: true,
            touchDragging: true,
            swingSpeed: 0.4,
            scrollSource: null,
            scrollBy:     1,
            scrollHijack: 100,
            scrollTrap:   false
        };
        $('#filter-4').find('.filter-top-select-content-list').css('height',innerWrapHeight);
        $('#filter-4').sly(options);
    };

    function setDirectionInTheRow (){
        $('.set-direction-row').each(function(){
            Number.prototype.Crop = function (x){
                var s = this+'', a = s.split('.');
                a[1]=a[1]||'';
                return parseFloat(a[0]+'.'+a[1].substring(0,x));
            };
            var rowWidth = $(this).width();
            var itemWidth = $(this).children('div').outerWidth();
            var itemInTheRow = (rowWidth/itemWidth).Crop(0);
            $(this).find('div').each(function(){
                var itemIndex = (($(this).index()));
                var rowNum = ((itemIndex/itemInTheRow).Crop(0)+1);
                $(this).removeClass('item-rtl');
                if (rowNum%2){
                    $(this).removeClass('item-rtl');
                } else {
                    $(this).addClass('item-rtl');
                }
            });
        });
    };

    $('.header-catalog-toggle').click(function(){
        if($(this).hasClass('active')){
            $(this).removeClass('active');
            $('.header-catalog-wrap').removeClass('active');
            $('.header-search-wrap').removeClass('active');
            $('.header-info-wrap').removeClass('active');
        } else {
            $('.header-navigation').removeClass('active');
            $('.header-navigation-toggle').removeClass('active');
            $(this).addClass('active');
            $('.header-catalog-wrap').addClass('active');
            $('.header-search-wrap').addClass('active');
            $('.header-info-wrap').addClass('active');
        }
    });
/*

    $('.header-navigation-toggle').click(function(){
        if($(this).hasClass('active')){
            $(this).removeClass('active');
            $('.header-navigation').removeClass('active');
        } else {
            $('.header-search-wrap').removeClass('active');
            $('.header-info-wrap').removeClass('active');
            $('.header-catalog-wrap').removeClass('active');
            $('.header-catalog-toggle').removeClass('active');
            $(this).addClass('active');
            $('.header-navigation').addClass('active');
        }
    });
*/

    $('.main-slider').slick({
        dots: true,
        infinite: true,
        slidesToShow: 1,
        adaptiveHeight: true,
        appendArrows:'.main-slider-arrows'
    });

    function listSly(){

        var options = {
            horizontal: true,
            itemNav: 'basic',
            speed: 300,
            scrollBar: $('.scrollbar'),
            dragHandle: true,
            dynamicHandle: true,
            minHandleSize: 50,
            syncSpeed: 0.5,
            mouseDragging: true,
            touchDragging: true,
            swingSpeed: 0.4,
            scrollSource: null,
            scrollBy:     1,
            scrollHijack: 100,
            scrollTrap:   false
        };

        $('.list-sly-frame').sly(options);

    };

    
    function gallerySly(){

        $('.detail-gallery-wrap').sly('reload');

        var options = {
            horizontal: true,
            itemNav: 'basic',
            speed: 300,
            scrollBar: $('.scrollbar'),
            dragHandle: true,
            dynamicHandle: true,
            minHandleSize: 50,
            syncSpeed: 0.5,
            mouseDragging: true,
            touchDragging: true,
            swingSpeed: 0.4,
            scrollSource: null,
            scrollBy:     1,
            scrollHijack: 100,
            scrollTrap:   false
        };
        $('.detail-gallery-wrap').sly(options);
    };
    
    if ( $('#detail-gallery').length > 0) {

        document.getElementById('detail-gallery').onclick = function (event) {
            event = event || window.event;
            var target = event.target || event.srcElement,
                link = target.src ? target.parentNode : target,
                options = {index: link, event: event},
                links = this.getElementsByTagName('a');
            blueimp.Gallery(links, options);
        };

    };

/*
    $('.select-color-item').click(function(){
        if($(this).hasClass('select-color-all')){
            if($(this).hasClass('active')){
                $(this).closest('.select-color').find('.select-color-item').removeClass('active');
            } else {
                $(this).closest('.select-color').find('.select-color-item').removeClass('active').addClass('active');
            }
        } else {
            $(this).toggleClass('active');
        }
    });
*/

    //functions on load and resize
    $(window).load(function () {

        stickyFooter();

        if ( $('.header-catalog>li.active').length > 0 ) {

            var e = $('.header-catalog>li.active');
            var eCoordX = e.offset().left;
            var eWidth = e.outerWidth();
            var eWidthCenter = eWidth/ 2;
            var eArrowHalfWidth = 18.5;
            var arrowCoordX = eCoordX + eWidthCenter - eArrowHalfWidth;
            var arrowLeft = arrowCoordX + 'px';

            $('.header-catalog-arrow').css('left', arrowLeft);
        }
        
        //headerCatalogArrow();
        headerCatalogSubMenuWidth();
        headerCatalogSubMenuItemDirection();
        //setDirectionInTheRow();
        //filterSly();
        gallerySly();
        listSly();
    });
    $(window).resize(function(){
        stickyFooter();
        headerCatalogArrow();
        headerCatalogSubMenuWidth();
        headerCatalogSubMenuItemDirection();
        setDirectionInTheRow();
        filterSly();
        gallerySly();
    });
});


function init_mask_phone(){
    $('input[type="tel"]').each(function(inx, el){
        new IMask(el,{mask: '+{7} (000) 000-00-00'});
    });
}
